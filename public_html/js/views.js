
YUI.add('mymodule', function (Y) {

    function alertMessage(msg) {
        if (msg)
            Y.one('#message').setHTML(Y.Escape.html(msg));
        else
            Y.one('#message').setHTML(Y.Escape.html(''));
    }


    Y.MyModuleMasterPage = Y.Base.create('MyModuleMasterPage', Y.View, [], {
        /**
         * this function is called to render the view
         * @returns {undefined}
         */
        render: function () {
            var context = this;
            var container = this.get('container');
            if (!container.inDoc()) {
                Y.one('#container').append(container);
            }
            var app = this.get('app');

            if (!app) {
                alertMessage('App instance is undefined');
            }

            // this can be loaded from a template file
            container.setHTML(Y.one('#recordListTemplate').html());
            var myDatasource = new Y.DataSource.Function({
                // request is an array
                source: function (request) {

                    var data = [
                        {id: 1, name: "Alice", score: 2.1},
                        {id: 2, name: "Bob", score: 1.6}
                    ];
                    return {"data": data};
                }
            });
            myDatasource.plug(Y.Plugin.DataSourceJSONSchema, {
                schema: {
                    resultListLocator: 'data',
                    resultFields: [
                        "id",
                        "name",
                        "score"
                    ]
                }
            });

            var table = new Y.DataTable(
                    {
                        columns: ['name', 'score']
                    });

            table.plug(Y.Plugin.DataTableDataSource, {
                datasource: myDatasource
            });

            table.render(container.one('#recordListTable'));
            table.set('id', 'nahovnoListTable');

            // for mobile, use rather tap
            table.delegate('click', function (e) {
                var record = table.getRecord(e.target);
                app.showView('detail', {"app": app, "record": record}, { "options.update": true} );
//                alertMessage('delegated on ' + JSON.stringify(record));
            }, "td");


            context.table = table;

            context.table.datasource.load({request: { /* anything passed as a request to the fn */}});
        }

    });

    Y.MyModuleDetailPage = Y.Base.create('MyModuleDetailPage', Y.View, [], {
        render: function () {

            try {
                var context = this;
                var container = this.get('container');
                if (!container.inDoc()) {
                    Y.one('#container').append(container);
                }
                var record = this.get('record');

                var app = this.get('app');

                if (!app) {
                    alertMessage('App instance is undefined');
                }

                if (!record) {
                    alert('No record passed');
                    return;
                }

                container.setHTML(Y.one('#recordDetailemplate').html());

                if (record) {
                    container.one('#nameField').set('value', record.get('name'));
                    container.one('#scoreField').set('value', record.get('score'));
                };
                
                container.one('#cancelButton').on('click', function() {
                    
                    app.showView('home', { "app": app} );
                    
                });

            } catch (e) {
                alert('MyModuleDetailPage render ' + e.message);
            }

        }
    });


},
        '0.0.1', // version
        {
            requires: [
                'view', 'node-load', 'datatable-base', 'model-list', 'escape',
                'node-event-delegate', 'datasource-function', 'datatable-datasource',
                'aui-datatable', 'event-tap', 'datasource-jsonschema'
            ]
        });
