
YUI().use(
        'node', 'app-base', 'mymodule',
        function (Y) {
             var app = new Y.App({
                    views: {
                        "home": {type: "MyModuleMasterPage"},
                        "detail": { type: "MyModuleDetailPage", parent: "home" }
                    },
                    root: "/",
                    serverRouting: false,
                    viewContainer: '#container',
                    container: '#container',
                    transitions: false,
                });

                app.route('/', function (req, res, next) {
                    app.showView('home', { "app": app, "options.update": true });
                });
                
                app.route('/detail/:recordId', function (req, res, next) {
                    var record = null;
                    // record = loadRecord(this.get('recordId'));
                    app.showView('detail', { "record": record} );
                });
                
                
                app.render().dispatch().save('/');
        });
